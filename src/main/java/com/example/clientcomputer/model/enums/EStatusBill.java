package com.example.clientcomputer.model.enums;

public enum EStatusBill {
	DRAFT, CONFIRMED, CANCELED
}
