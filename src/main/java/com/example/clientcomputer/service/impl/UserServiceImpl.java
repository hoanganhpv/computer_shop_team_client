package com.example.clientcomputer.service.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.clientcomputer.model.UserLogin;
import com.example.clientcomputer.model.Users;
import com.example.clientcomputer.model.enums.EUser;
import com.example.clientcomputer.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private RestTemplate restTemplate;

	public UserServiceImpl(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	@Override
	public String signUp(Users users) {
		users.setRoleId((long) EUser.USER.getValue());
		HttpEntity<Users> requestBody = new HttpEntity<>(users);
		ResponseEntity<Users> responseEntity = restTemplate.postForEntity("/user", requestBody, Users.class);
		if (responseEntity.getStatusCode()==HttpStatus.OK) {
			return "redirect:login";
		} else {
			return "redirect:login";			
		}
	}

	@Override
	public String login(UserLogin users, HttpSession session) {
		HttpEntity<UserLogin> requestBody = new HttpEntity<>(users);
		ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8081/login", requestBody, String.class);
		if(responseEntity.getStatusCode() == HttpStatus.OK) {
			System.out.println(responseEntity.getHeaders());
			session.setAttribute("token", responseEntity.getHeaders().get("Authorization").get(0));
			return "redirect:/";
		}
		
		return "web/login?message=invalid";
	}

	@Override
	public Users findById(Long id) {
		ResponseEntity<Users> responseEntity = restTemplate.getForEntity("/user/"+id, Users.class);
		return responseEntity.getBody();
	}
	
	@Override
	public List<Users> findAllUsers(HttpSession session) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", (String) session.getAttribute("token"));
		HttpEntity<List<Users>> requestBody = new HttpEntity<>(headers);
		ResponseEntity<List<Users>> responseEntity= restTemplate.exchange("/user", HttpMethod.GET, requestBody, new ParameterizedTypeReference<List<Users>>(){});
		return responseEntity.getBody();
	}
}
