package com.example.clientcomputer.service.impl;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.clientcomputer.model.Import;
import com.example.clientcomputer.model.ImportDetail;
import com.example.clientcomputer.service.ImportService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ImportServiceImpl implements ImportService{
	private RestTemplate restTemplate;
	
	@Override
	public void save(Import importObject, ImportDetail details) {
		HttpEntity<Import> entity = new HttpEntity<Import>(importObject);
		ResponseEntity<Import> responseEntity = restTemplate.postForEntity("/import", entity, Import.class);
		
		
	}
}
