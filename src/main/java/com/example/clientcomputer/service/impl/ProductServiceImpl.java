package com.example.clientcomputer.service.impl;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.example.clientcomputer.model.Cart;
import com.example.clientcomputer.model.Product;
import com.example.clientcomputer.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	
	private RestTemplate restTemplate;
	
	public ProductServiceImpl(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}

	@Override
	public String save(Product product, MultipartFile imageLink) {
		try {
			product.setImage(Base64.getEncoder().encodeToString(imageLink.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
			return "redirect:admin-product-add";
		}
		HttpEntity<Product> httpEntity = new HttpEntity<>(product);
		ResponseEntity<Product> responseEntity;
		if(product.getId()!=null) {
			responseEntity = restTemplate.exchange("/product",
										HttpMethod.PUT, httpEntity,
								        Product.class);
		} else {
			responseEntity = restTemplate.postForEntity("/product", httpEntity, Product.class);
		}
		if(responseEntity.getStatusCode()==HttpStatus.OK) {
			return "redirect:admin-product";			
		}else {
			return "common/500error";
		}
	}

	@Override
	public Product findById(Long id) {
		ResponseEntity<Product> responseEntity = restTemplate.getForEntity("/product/"+id, Product.class, id);
		
		if(responseEntity.getStatusCode()==HttpStatus.INTERNAL_SERVER_ERROR) {
			return null;
		} else if(responseEntity.getStatusCode()==HttpStatus.OK)  {
			return responseEntity.getBody();
		} else {
			return null;
		}
	}

	@Override
	public List<Product> findRelatedProduct(Long categoryId) {
		ResponseEntity<List<Product>> responseEntity = restTemplate.exchange("/product/relate?categoryId="+categoryId, HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});;
		return responseEntity.getBody();
	}

	@Override
	public List<Product> findByCategoryId(Long categoryId) {
		ResponseEntity<List<Product>> responseEntity = restTemplate.exchange("/product/?categoryId="+categoryId, HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});;
		return responseEntity.getBody();
	}

	@Override
	public String addToCart(Long productId) {
		Cart cart = Cart.builder()
						.productId(productId)
						.userId(1L)
						.quantity(1)
						.price(findById(productId).getPrice())
						.build();
		HttpEntity<Cart> httpEntity = new HttpEntity<Cart>(cart);
		ResponseEntity<Cart> responseEntity;
		try {
			responseEntity = restTemplate.postForEntity("/cart", httpEntity, Cart.class);
		} catch (Exception e) {
			return "redirect:/login";
		}
		if(responseEntity.getStatusCode()==HttpStatus.FORBIDDEN) {
			return "redirect:/login";
		}else if(responseEntity.getStatusCode()==HttpStatus.OK) {
			return "redirect:/cart";
		}
		return "redirect:product/detail/"+productId;
	}
	
	@Override
	public List<Cart> getCartByUserId(Long userId) {
		ResponseEntity<List<Cart>> responseEntity;
		try {
			responseEntity = restTemplate.exchange("/cart/"+userId, HttpMethod.GET, null, new ParameterizedTypeReference<List<Cart>>() {});	
		} catch (Exception e) {
			return null;
		}
		return responseEntity.getBody();
	}
	
	@Override
	public String deleteItem(Long productId, Long userId) {
		restTemplate.delete(String.format("/cart?productId=%d&userId=%d", productId, userId));
		
		return "redirect:/cart";
	}
	
	@Override
	public String updateQuantityItem(Long productId, Long userId, Integer quantity) {
		Cart cart = Cart.builder()
				.productId(productId)
				.userId(userId)
				.quantity(quantity)
				.build();
		restTemplate.put("/cart", cart);
		
		return "redirect:/cart";
	}
	
	@Override
	public Long getAmount(List<Cart> carts) {
		Long amount = 0L;
		for (Cart cart : carts) {
			amount = (long) (amount + (cart.getQuantity()*cart.getPrice()));
		}
		return amount;
	}
}
