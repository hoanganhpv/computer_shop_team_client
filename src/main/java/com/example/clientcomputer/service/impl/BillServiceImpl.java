package com.example.clientcomputer.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.clientcomputer.model.Bill;
import com.example.clientcomputer.model.Cart;
import com.example.clientcomputer.model.enums.EStatusBill;
import com.example.clientcomputer.service.BillService;
import com.example.clientcomputer.service.ProductService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BillServiceImpl implements BillService{
	private ProductService productService;
	private RestTemplate restTemplate;

	@Override
	public String saveBill(Bill bill) {
		List<Cart> carts = productService.getCartByUserId(1L);
		bill.setUserId(1L);
		bill.setAmount(productService.getAmount(carts));
		bill.setCreateDate(LocalDateTime.now());
		bill.setStatus(EStatusBill.DRAFT);
		
		HttpEntity<Bill> httpEntity = new HttpEntity<Bill>(bill);
		ResponseEntity<Bill> responseEntity = restTemplate.postForEntity("/bill", httpEntity, Bill.class);
		if(responseEntity.getStatusCode() == HttpStatus.OK) {
			return "redirect:/";
		}
		
		return "redirect:/checkout";
	}

	@Override
	public List<Bill> getBills(HttpSession session) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", (String)session.getAttribute("token"));
		HttpEntity<List<Bill>> httpEntity = new HttpEntity<List<Bill>>(headers);
		ResponseEntity<List<Bill>> responseEntity = restTemplate.exchange("/bill", HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<Bill>>() {});
		return responseEntity.getBody();
	}

}
