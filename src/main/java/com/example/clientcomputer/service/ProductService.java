package com.example.clientcomputer.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.example.clientcomputer.model.Cart;
import com.example.clientcomputer.model.Product;

public interface ProductService {

	String save(Product product, MultipartFile imageLink);

	Product findById(Long id);

	List<Product> findRelatedProduct(Long categoryId);

	List<Product> findByCategoryId(Long categoryId);

	String addToCart(Long productId);

	List<Cart> getCartByUserId(Long userId);

	String deleteItem(Long productId, Long userId);

	String updateQuantityItem(Long productId, Long userId, Integer quantity);
	
	Long getAmount(List<Cart> carts);

}
