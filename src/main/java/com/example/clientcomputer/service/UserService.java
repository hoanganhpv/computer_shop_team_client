package com.example.clientcomputer.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.clientcomputer.model.UserLogin;
import com.example.clientcomputer.model.Users;

public interface UserService {

	String signUp(Users users);

	String login(UserLogin users, HttpSession session);

	Users findById(Long id);

	List<Users> findAllUsers(HttpSession session);

}
