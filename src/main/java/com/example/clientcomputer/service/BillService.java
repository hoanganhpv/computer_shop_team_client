package com.example.clientcomputer.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.clientcomputer.model.Bill;

public interface BillService {
	String saveBill(Bill bill);
	
	List<Bill> getBills(HttpSession session);
}
