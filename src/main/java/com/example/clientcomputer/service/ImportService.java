package com.example.clientcomputer.service;

import com.example.clientcomputer.model.Import;
import com.example.clientcomputer.model.ImportDetail;

public interface ImportService {

	void save(Import importObject, ImportDetail details);

}
