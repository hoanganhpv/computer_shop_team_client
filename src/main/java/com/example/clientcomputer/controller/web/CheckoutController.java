package com.example.clientcomputer.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.clientcomputer.model.Bill;
import com.example.clientcomputer.service.BillService;
import com.example.clientcomputer.service.UserService;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("checkout")
@AllArgsConstructor
public class CheckoutController {
	private UserService userService;
	private BillService billService;
	
	@GetMapping
	public String getCheckout(Model model) {
		model.addAttribute("user", userService.findById(1L));
		model.addAttribute("bill", new Bill());
		return "web/checkout";
	}
	
	@PostMapping
	public String saveBill(@ModelAttribute Bill bill) {
		return billService.saveBill(bill);
	}
}
