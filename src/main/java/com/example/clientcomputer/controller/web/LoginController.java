package com.example.clientcomputer.controller.web;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.clientcomputer.model.UserLogin;
import com.example.clientcomputer.model.Users;
import com.example.clientcomputer.service.UserService;

@Controller
public class LoginController {
	private UserService userService;
	
	public LoginController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping("/login")
	public String getLogin(Model model) {
		model.addAttribute("userLogin", new UserLogin());
		return "web/login";
	}
	
	@GetMapping("/register")
	public String getRegister(Model model) {
		model.addAttribute("userSignup", new Users());
		return "web/register";
	}
	
	@PostMapping("/signup")
	public String signup(@Valid @ModelAttribute("userSignup") Users userSignup,
						BindingResult result) {
		if (result.hasErrors()) {
		    return "web/register";
		}
		return userService.signUp(userSignup);
	}
	
	@PostMapping("/login")
	public String login(@Valid @ModelAttribute("userLogin") UserLogin userLogin,
						BindingResult result,
						HttpSession session) {
		if (result.hasErrors()) {
		    return "web/login";
		}
		return userService.login(userLogin, session);
	}
	
	@GetMapping("/logout")
	public String logOut(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}
}
