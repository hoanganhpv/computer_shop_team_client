package com.example.clientcomputer.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.clientcomputer.model.Product;
import com.example.clientcomputer.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController {
	private ProductService productService;
	
	public ProductController(ProductService productService) {
		super();
		this.productService = productService;
	}

	@GetMapping
	public String getAll(@RequestParam(name = "categoryId", required = false) Long id,
			@RequestParam(name="page",required = false) Integer page, Model model) {
		if(id!=null) {
			model.addAttribute("products", productService.findByCategoryId(id));
		}
		return "web/store";
	}
	
	@GetMapping("/detail/{id}")
	public String getOneProduct(@PathVariable Long id, Model model) {
		Product product = productService.findById(id);
		if(product==null) {
			return "common/500error";
		}
		model.addAttribute("product", product);
		model.addAttribute("productRelated", productService.findRelatedProduct(product.getCategoryId()));
		return "web/product";
	}
}
