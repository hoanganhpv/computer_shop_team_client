package com.example.clientcomputer.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	@GetMapping
	public String getHome() {
		return "web/index";
	}
	
	@GetMapping("/blank")
	public String getBlank() {
		return "web/blank"; 
	}
}
