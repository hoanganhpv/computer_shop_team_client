package com.example.clientcomputer.controller.advice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.RestTemplate;

import com.example.clientcomputer.model.Cart;
import com.example.clientcomputer.model.Category;
import com.example.clientcomputer.model.Product;

@ControllerAdvice
public class GlobalControllerAdvice {
	@Autowired
	private RestTemplate restTemplate;
	
	@ModelAttribute("categories")
	public List<Category> getCategories(){
		ResponseEntity<List<Category>> entity = restTemplate.exchange("/category", HttpMethod.GET, null, new ParameterizedTypeReference<List<Category>>() {});
		List<Category> categories = entity.getBody().stream().map(t -> {
			ResponseEntity<List<Product>> productEntity = restTemplate.exchange("/product/category"+t.getId(), HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
			t.setProductNo(productEntity.getBody().size());
			return t;
		}).toList();
		return categories;
	}
	
	@ModelAttribute(name = "products")
	public List<Product> getProducts(){
		ResponseEntity<List<Product>> entity = restTemplate.exchange("/product/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
		return entity.getBody();
	}
	
	@ModelAttribute(name = "carts")
	public List<Cart> getCarts(Model model){
		Long userId = 1L;
		ResponseEntity<List<Cart>> entity ;
		try {
			entity = restTemplate.exchange("/cart/"+userId, HttpMethod.GET, null, new ParameterizedTypeReference<List<Cart>>() {});			
		} catch (Exception e) {
			return null;
		}
		Long amount=0L;
		Integer totalItems=0;
		for (Cart cart : entity.getBody()) {
			amount = (long) (amount + (cart.getQuantity()*cart.getPrice()));
			totalItems = totalItems + cart.getQuantity();
		};
		model.addAttribute("amount", amount);
		model.addAttribute("totalCartItems", totalItems);
		
		return entity.getBody();
	}
}
