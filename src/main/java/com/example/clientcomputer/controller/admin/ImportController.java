package com.example.clientcomputer.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.clientcomputer.model.Import;
import com.example.clientcomputer.model.ImportDetail;
import com.example.clientcomputer.service.ImportService;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("admin")
@AllArgsConstructor
public class ImportController {
	private ImportService importService;
	
	@GetMapping("/admin-product-import")
	public String getImportView(Model model) {
		Import import1 = new Import();
		ImportDetail detail = new ImportDetail();
		model.addAttribute("import", import1);
		model.addAttribute("details", detail);
		return "admin/productimport";
	}
	
	@PostMapping("/admin-product-import")
	public String addImportDetail(@ModelAttribute("import") Import importObject,
								@ModelAttribute("details") ImportDetail details) {
		System.out.println(importObject);
		System.out.println(details);
		importService.save(importObject, details);
		
		return "redirect:/admin/admin-product-import";
	}
}
