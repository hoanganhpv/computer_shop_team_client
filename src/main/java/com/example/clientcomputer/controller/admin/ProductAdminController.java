package com.example.clientcomputer.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.example.clientcomputer.model.Category;
import com.example.clientcomputer.model.Product;
import com.example.clientcomputer.service.ProductService;

@Controller
@RequestMapping("/admin/")
public class ProductAdminController {
	private RestTemplate restTemplate;
	private ProductService productService;
	
	public ProductAdminController(RestTemplate restTemplate, ProductService productService) {
		super();
		this.restTemplate = restTemplate;
		this.productService = productService;
	}

	@GetMapping("/admin-product")
	public String getProductAdmin() {
		return "admin/product";
	}
	
	@GetMapping("/admin-product-add")
	public String getAddProduct(Model model) {
		model.addAttribute("product", new Product());
		return "admin/addproduct";
	}
	
	@PostMapping("/admin-product-add")
	public String addProduct(@ModelAttribute("product") Product product,
			@RequestParam("image_link") MultipartFile imageLink) {
		return productService.save(product, imageLink);
	}
	
	@GetMapping("/admin-product-edit")
	public String editProduct(@RequestParam("id") Long id, Model model) {
		ResponseEntity<Product> responseEntity = restTemplate.getForEntity("/product/"+id, Product.class);
		model.addAttribute("product", responseEntity.getBody());
		return "admin/addproduct";
	}
	
	@GetMapping("/admin-product-delete/{id}")
	public String deleteProduct(@PathVariable("id") Long id) {
		restTemplate.delete("/product/"+id, id);
		return "admin/product";
	}
	
	@GetMapping("/admin-category")
	public String getCategory(Model model) {
		model.addAttribute("categoryNew", new Category());
		return "admin/category";
	}
	
	@PostMapping("/admin-category")
	public String addCategory(@ModelAttribute("categoryNew") Category category, HttpSession session) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", (String) session.getAttribute("token"));
		HttpEntity<Category> requestBody = new HttpEntity<>(category, headers);
		ResponseEntity<Category> responseEntity= restTemplate.postForEntity("/category", requestBody, Category.class);
		if(responseEntity.getStatusCode() == HttpStatus.OK) {
			return "redirect:admin-category";
		}else {
			return "common/500error";
		}
	}
	
	@GetMapping("/admin-category-add")
	public String getAddCategory() {
		return "admin/addcategory";
	}
}
