package com.example.clientcomputer.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.clientcomputer.service.BillService;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("admin/")
@AllArgsConstructor
public class OrderAdminController {
	private BillService billService;
	
	@GetMapping("admin-order")
	public String getOrder(Model model, HttpSession session) {
		model.addAttribute("bills", billService.getBills(session));
		return "admin/order";
	}
	
	@GetMapping("admin-order-add")
	public String getAddOrder() {
		return "admin/addorder";
	}
	
	@GetMapping("admin-cancel")
	public String getCancelOrder() {
		return "admin/cancel";
	}
	
	@GetMapping("api-order-detail")
	public String getOrderDetail() {
		return "admin/orderdetail";
	}
}
